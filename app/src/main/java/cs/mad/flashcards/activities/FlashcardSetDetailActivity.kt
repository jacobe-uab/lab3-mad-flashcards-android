package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetDetailAdapter
import cs.mad.flashcards.entities.Flashcard

class FlashcardSetDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flashcard_set_detail)

        var flashcards = Flashcard("N/A", "N/A").getHardcodedFlashcards()

        val setsRecyclerView = findViewById<RecyclerView>(R.id.sets_detail_view)
        setsRecyclerView.adapter = FlashcardSetDetailAdapter(flashcards)

        val linearLayout = LinearLayoutManager(this)
        setsRecyclerView.layoutManager = linearLayout

        val addButton = findViewById<Button>(R.id.sets_detail_add)
        val deleteButton = findViewById<Button>(R.id.sets_detail_delete)
        val title = findViewById<TextView>(R.id.sets_detail_title)

        title.text = "Set 1"

        val adapter = setsRecyclerView.adapter
        addButton.setOnClickListener {
            flashcards.add(flashcards.size, Flashcard("I'm new!", "Hello!"))
            adapter?.notifyItemInserted(flashcards.size - 1)
        }

        deleteButton.setOnClickListener {
            if (flashcards.size > 0) {
                flashcards.removeAt(flashcards.size - 1)
                adapter?.notifyItemRemoved(flashcards.size)
            }
        }

        val swipeHandler = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                flashcards.removeAt(viewHolder.adapterPosition)
                adapter?.notifyItemRemoved(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(setsRecyclerView)


    }
}