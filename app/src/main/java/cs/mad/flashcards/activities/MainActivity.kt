package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.FlashcardSet

/*
===================================================================================================================

     Reference documentation for recyclers: https://developer.android.com/guide/topics/ui/layout/recyclerview

===================================================================================================================
 */

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val flashcardSets = FlashcardSet("N/A").getHardcodedFlashcardSets()

        val setsRecyclerView = findViewById<RecyclerView>(R.id.sets_view)
        setsRecyclerView.adapter = FlashcardSetAdapter(flashcardSets)

        val gridLayout = GridLayoutManager(this, 2)
        setsRecyclerView.layoutManager = gridLayout

        val addButton = findViewById<Button>(R.id.sets_add)
        val deleteButton = findViewById<Button>(R.id.sets_delete)

        val adapter = setsRecyclerView.adapter

        addButton.setOnClickListener {
            flashcardSets.add(flashcardSets.size, FlashcardSet("I'm new!"))
            adapter?.notifyItemInserted(flashcardSets.size - 1)
        }

        deleteButton.setOnClickListener {
            flashcardSets.removeAt(flashcardSets.size - 1)
            adapter?.notifyItemRemoved(flashcardSets.size)
        }
    }
}