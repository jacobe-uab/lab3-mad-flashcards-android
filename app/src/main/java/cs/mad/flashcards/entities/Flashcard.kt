package cs.mad.flashcards.entities


data class Flashcard(
    val question: String,
    val answer: String
) {

    fun getHardcodedFlashcards(): MutableList<Flashcard> {
        return mutableListOf(
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
            Flashcard("Term 1", "Def 1"),
        )
    }
}