package cs.mad.flashcards.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.entities.FlashcardSet

class FlashcardSetAdapter(val dataSet: List<FlashcardSet>) :
    RecyclerView.Adapter<FlashcardSetAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val buttonView: Button = view.findViewById(R.id.set_button)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard_set, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.buttonView.text = item.title
        viewHolder.buttonView.setOnClickListener {
            viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context, FlashcardSetDetailActivity::class.java))
        }

    }

    override fun getItemCount(): Int {
        return dataSet.size
    }
}